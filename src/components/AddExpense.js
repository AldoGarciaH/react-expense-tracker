import { TextField, Input, Select, MenuItem, InputAdornment, Button, Card, Typography, Grid, InputLabel, FormControl } from "@mui/material";
import { useState } from 'react';
import categories from "../config/config";
import '../styles/AddExpense.css';


function AddExpense({ addExpense }) {
  const [name, setName] = useState('');
  const [cost, setCost] = useState(0);
  const [itemCategory, setitemCategory] = useState('');

  const submit = () => {
    addExpense(name, cost, itemCategory);
    setName('');
    setCost(0);
    setitemCategory('');
  }

  return (
    <Card className="add-expense-container" sx={{ backgroundColor: "#b5deff" }}>
      <Typography variant="h5" gutterBottom>Add new expense</Typography>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={2} className="add-expense-items">
        <Grid item xs={2} sx={{ marginTop: 2 }}>
          <TextField className="name" placeholder="Item name" variant="standard" value={name} onChange={(e) => setName(e.target.value)} />

        </Grid>
        <Grid item xs={2}>
          <Input
            className="cost"
            type="number"
            placeholder="Cost"
            variant="standard"
            onChange={(e) => setCost(e.target.value)}
            value={cost ? cost : ''}
            startAdornment={<InputAdornment position="start">$</InputAdornment>}
          />
        </Grid>
        <Grid item xs={2}>
          <FormControl>
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select variant="outlined" labelId="demo-simple-select-label" className="categories" label="Category" value={itemCategory} onChange={(e) => setitemCategory(e.target.value)}>
              {
                categories.map((category, index) => {
                  return <MenuItem value={category} key={index}>{category}</MenuItem>
                })
              }
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={2}>
          <Button className="add" variant="contained" sx={{ backgroundColor: "#005eaa" }} onClick={() => submit()}>Add</Button>
        </Grid>
      </Grid>
    </Card>
  );
}

export default AddExpense;