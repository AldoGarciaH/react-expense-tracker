import { useEffect, useState } from "react";
import { Divider, Grid, Typography, Card } from "@mui/material";
import '../styles/ExpenseCard.css';

function ExpenseCard({list, expenses}) {

  const [total, setTotal] = useState(0);

  const divider = <Divider sx={{ borderBottomWidth: 5 }}/>;
  
  useEffect(()=> {
    let add = 0;
    list.items?.map(item => add += parseFloat(item.cost));
    setTotal(add);
  },[expenses, list]);

  return (
    <Card className="list-container" sx={{backgroundColor: "#fffeb3"}}>
      <Typography variant="h6">{list.category}</Typography>
      {
        list.items?.map((item, index)=>{
          return <div className="item-container" key = {index}>
            <Grid container spacing={4}>
              <Grid item xs={6} sx={{marginY: 1}}>{item.name}</Grid>
              <Grid item xs={6} sx={{marginY: 1}}>${item.cost}</Grid>
            </Grid>
            {(list.items.length > 1 && index < list.items.length - 1 ) && divider}
          </div>
        })
      }
      <div className="total-bottom">
        {divider}
        <h3>Total: ${total}</h3>
      </div>
      
    </Card>
  );
}

export default ExpenseCard;