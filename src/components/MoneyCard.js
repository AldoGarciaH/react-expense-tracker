import { Card, Typography } from "@mui/material"

function MoneyCard({label, value, sx}) {
  return (
    <Card sx={sx}>
      <Typography variant="h6" gutterBottom marginTop={2}>{label}</Typography>
      <h2>${value}</h2>
    </Card>
  );
}

export default MoneyCard;
