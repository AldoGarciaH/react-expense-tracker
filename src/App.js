import './App.css';
import { Input, InputAdornment, Grid, Typography } from '@mui/material';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { useEffect, useState } from 'react';
import MoneyCard from './components/MoneyCard';
import AddExpense from './components/AddExpense';
import ExpenseCard from './components/ExpenseCard';

function App() {
  const [budget, setBudget] = useState(0);
  const [spent, setSpent] = useState(0);
  const [remaining, setRemaining] = useState(0);
  const [expenses, setExpenses] = useState([]); //{category: "food", items: [{name: "cheezburger", cost:100}]}

  useEffect(()=>{
    setRemaining(parseFloat(budget) - parseFloat(spent));
  },[budget, spent]);

  const addExpense = (name, cost, category) => {
    if(!(name && cost && category)){ // validating values
      alert("Must fill all fields")
    } else {

      let newExpenses = [...expenses];
      const categoryIndex = expenses.findIndex(e => e.category === category);
      
      if(categoryIndex !== -1){ // Category is already added

        newExpenses[categoryIndex].items.push({name: name, cost:cost});

      } else { // Add as a new category

        newExpenses.push({
          category: category,
          items: [{name: name, cost: cost}]
        });
      }

      setExpenses(newExpenses);
      setSpent(parseFloat(spent) + parseFloat(cost));
    }
  };

  const getRemainingColor = () => {
    const range = parseInt((parseFloat(spent)/parseFloat(budget))*10);

    if(range <= 1){
      return "#63ff55";
    }

    if(range >= 10){
      return "#ff6b6b";
    }

    switch (range){
      case 2:
        return "#8FFF55";
      case 3:
        return "#A3FF55";
      case 4:
        return "#C9FF55";
      case 5:
        return "#E4FF55";
      case 6:
        return "#FFE355";
      case 7:
        return "#FFCD55";
      case 8:
        return "#FFA755";
      case 9:
        return "#FF8455";
      default: 
        return "#FFFFFF";
    }
  }

  return (
    <div className="App">
      <header className="App-header">
      <Typography variant="h2" gutterBottom>EXPENSE TRACKER</Typography>
       <Input 
        type="number" 
        placeholder="Budget" 
        variant="standard" 
        onChange={(e)=> e.target.value !== "" ? setBudget(e.target.value) : setBudget(0)} 
        value={budget?budget:''} 
        startAdornment={<InputAdornment position="start">$</InputAdornment>}
      />
      </header>
      <Grid container spacing={2} className="values">
        <Grid item xs={4}><MoneyCard sx={{ backgroundColor: "#63ff55" }} id="initial-budget" label="Initial budget" value={budget}/></Grid>
        <Grid item xs={4}><MoneyCard sx={{ backgroundColor: ()=> getRemainingColor() }} id="remaining" label="Remaining" value={remaining + (remaining < 0 && " ☠")}/></Grid>
        <Grid item xs={4}><MoneyCard sx={{ backgroundColor: "#ff6b6b" }} id="spent" label="Spent" value={spent}/></Grid>
      </Grid>
      <div>
        <AddExpense addExpense={addExpense}/>
      </div>
      <Grid 
        container
        direction="row"
        justifyContent="space-evenly"
        alignItems="flex-start" 
        spacing={1}
        sx={{marginTop: 1}}>
        {
          expenses.map((list, index) => {
            return <Grid item xs={4}><ExpenseCard list={list} key={index} expenses={expenses}/></Grid>
        })}
      </Grid>
    </div>
  );
}

export default App;